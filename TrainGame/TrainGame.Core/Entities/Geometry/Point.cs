﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainGame.Core.Entities.Geometry
{
    public class Point
    {
        public int X { get; }
        public int Y { get; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Point GetDrawingLocation()
        {
            return this * Config.BlockWidth;
        }

        public static Point operator +(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static Point operator +(Point p1, int d)
        {
            return new Point(p1.X + d, p1.Y + d);
        }

        public static Point operator *(Point p1, int d)
        {
            return new Point(p1.X * d, p1.Y * d);
        }

        public static Point Middle(Point p1, Point p2)
        {
            return new Point((p1.X + p2.X) / 2, (p1.Y + p2.Y) / 2);
        }

        public override string ToString()
        {
            return "(" + X + ";" + Y + ")";
        }
    }
}
