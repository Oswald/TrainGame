﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Enum;
using TrainGame.Core.Entities.Geometry;
using TrainGame.Core.Entities.Map;

namespace TrainGame.Core.Entities.Tracks
{
    /// <summary>
    /// This class holds information of all tracks, contructions and etc
    /// </summary>
    public class TrackHandler
    {
        public List<TrackPiece> TrackPieces { get; } = new List<TrackPiece>();
        
        Block[,] Map { get; }

        public TrackHandler(Block[,] map)
        {
            Map = map;
        }

        public TrackPiece ConstructTrack(int x, int y, Direction direction)
        {
            var piece = new TrackPiece(new Point(x, y), direction);
            Map[x, y].AddTrack(piece);
            TrackPieces.Add(piece);
            return piece;
        }

        Block GetBlock(int x, int y, Direction d)
        {
            return Map[GetX(x, d), GetY(y, d)];
        }

        int GetX(int x, Direction direction)
        {
            return direction.VectorDirection().X + x;
        }

        int GetY(int y, Direction direction)
        {
            return direction.VectorDirection().Y + y;
        }

        public List<Direction> GetConnected(int x, int y, Direction direction)
        {
            return DirectionMethods.AllDirections
                .Where(d => d.Difference(direction) <= 1)
                .Where(searched => GetBlock(x, y, searched).GetTrack(searched) != null)
                .ToList();
             
        }

        public Route FindRoute(TrackPiece start, TrackPiece target)
        {
            
            /*
            routes.Add(NextStep(new Dictionary<TrackNode, int>(), 0, start.A, target.A));
            routes.Add(NextStep(new Dictionary<TrackNode, int>(), 0, start.A, target.B));
            routes.Add(NextStep(new Dictionary<TrackNode, int>(), 0, start.B, target.A));
            routes.Add(NextStep(new Dictionary<TrackNode, int>(), 0, start.B, target.B));
                       var succesfull = routes.Where(t => t != null).ToList();
            if (succesfull.Count() == 0)
            {
                return null;
            }
            else {
                return succesfull.OrderBy(t => t.Count)?.First();
            }
            */

            throw new NotImplementedException("Not implemented");
        }


        object GenerateKey(int X, int Y, Direction Direction)
        {
            return new
            {
                X,
                Y,
                Direction
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Steps">TrackPiece and direction to string as key</param>
        /// <param name="start"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        LinkedList<Direction> NextStep(Dictionary<object, double> Steps, double travelled, int currentX, int currentY, int goalX, int goalY, Direction direction)
        {
            
            var connected = GetConnected(currentX, currentY, direction);

            foreach(var dir in connected)
            {
                var key = GenerateKey(GetX(currentX, dir), GetY(currentY, dir), dir);
                
                if(Steps.ContainsKey(key) && Steps[key] < travelled)
                {
                    Steps[key] = travelled;
                }
            }
            /*
            LinkedList<TrackNode> route = null;
            if (start.Connections.Contains(target))
            {
                route = new LinkedList<TrackNode>();
                route.AddFirst(start);
                route.AddLast(target);
                return route;
            }
            else
            {
                foreach(var node in start.Connections)
                {
                    if(!Steps.ContainsKey(node) || Steps[node] > CurrentSteps)
                    {
                        Steps[node] = CurrentSteps;
                        var attempt = NextStep(Steps, CurrentSteps + 1, node, target);
                        
                        if (attempt != null && (route == null || attempt.Sum(s => s.Direction.Length()) < route.Sum(s => s.Direction.Length())))
                        {
                            route = attempt;
                        }
                    }
                }
                route?.AddFirst(start);
                return route;
            }
            
            */
            throw new NotImplementedException("Not implemented");
            
        }

        

        

        
    }
}
