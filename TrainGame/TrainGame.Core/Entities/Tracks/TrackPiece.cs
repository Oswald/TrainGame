﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Drawing;
using TrainGame.Core.Entities.Enum;
using TrainGame.Core.Entities.Geometry;

namespace TrainGame.Core.Entities.Tracks
{
    public class TrackPiece : IDrawableEntity
    {
        public Point Location { get; }
        public Direction Direction { get; }

        public TrackPiece(Point location , Direction direction)
        {
            Location = location;
            Direction = direction;
        }

       

        public override string ToString()
        {
            return "(" + Location.X + ";" + Location.Y + "), " + Direction.ToString();
        }

        public DrawingInfo GetDrawingInfo()
        {
            return new TrackDrawingInfo(Location, Direction.RadianDirection(), 1, Direction.Diagonal());
        }
    }
}
