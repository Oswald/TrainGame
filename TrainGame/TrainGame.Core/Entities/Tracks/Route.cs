﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Enum;
using TrainGame.Core.Entities.Geometry;

namespace TrainGame.Core.Entities.Tracks
{
    public class Route
    {
        /// <summary>
        /// Direction, distance where next direction begins and track for later use (f. ex. checking if track is occupied)
        /// </summary>
        List<Tuple<Direction, double, TrackPiece>> Steps = new List<Tuple<Direction, double, TrackPiece>>();

        public double Length { get; private set; } = 0;

        public void AddStep(Direction direction, TrackPiece piece)
        {
            if(Steps.Last().Item1.Difference(direction) <= 1)
            {
                Length += direction.Length();
                Steps.Add(new Tuple<Direction, double, TrackPiece>(direction, Length, piece));
            }
            else
            {
                throw new Exception("Too much difference between directions: " + Steps.Last().Item1.ToString() + ", " + direction.ToString());
            }
        }

        public int GetIndex(int currentIndex, double travelledDistance)
        {
            if (travelledDistance < Steps.Last().Item2)
            {
                return currentIndex + 1;
            }
            return currentIndex;
        }

        public Direction GetCurrentDirection(int index, double distance)
        {
            
            return Steps[index].Item1;
        }

        /// <summary>
        /// Returns travelled distance on current track piece
        /// </summary>
        /// <param name="distance"></param>
        /// <returns></returns>
        public double GetDistanceOnCurrentStep(int index, double distance)
        {
            return Steps[index].Item2 - distance;
            
        }

        public bool Unfinished(double index)
        {
            return index < Steps.Count();
        }

        /*
        public class RouteComparer<TrackNode> : IComparer<TrackNode>
        {
            Dictionary<TrackNode, double> Distances { get; }

            public RouteComparer(Dictionary<TrackNode, double> distances)
            {
                Distances = distances;
            }

            public int Compare(TrackNode x, TrackNode y)
            {
                if (Distances[x] < Distances[y])
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
        }
        */

    }

}
