﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Drawing;
using TrainGame.Core.Entities.Enum;
using TrainGame.Core.Entities.Tracks;

namespace TrainGame.Core.Entities.Trains
{
    public class TrainCar : IDrawableEntity
    {
        Route CurrentRoute { get; set; }
        int RouteIndex { get; set; }
        double TotalDistance { get; set; }
        double Speed { get; set; } = 0.02;

        double X;
        double Y;
        
        public void SetRoute(Route route)
        {
            CurrentRoute = route;
            TotalDistance = 0;
            RouteIndex = 0;
        }



        public void Update(double elapsedMilliseconds)
        {
            if (CurrentRoute.Unfinished(RouteIndex))
            {
                TotalDistance += Speed * elapsedMilliseconds;
            }
            RouteIndex = CurrentRoute.GetIndex(RouteIndex, TotalDistance);
        }

        public DrawingInfo GetDrawingInfo()
        {
            if (CurrentRoute.Unfinished(RouteIndex)) {
                return new DrawingInfo(X, Y, CurrentRoute.GetCurrentDirection(RouteIndex, TotalDistance).RadianDirection(), 1);
            }
            else
            {
                return null;
            }
        }

    }
}
