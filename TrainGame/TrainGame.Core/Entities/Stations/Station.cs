﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Tracks;

namespace TrainGame.Core.Entities.Stations
{
    public class Station
    {
        public TrackPiece Track { get; }

        public Station(TrackPiece track)
        {
            Track = track;
        }
    }
}
