﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Enum;
using TrainGame.Core.Entities.Tracks;

namespace TrainGame.Core.Entities.Map
{
    public class Block
    {
        BlockType Type { get; set; }
        List<TrackPiece> Tracks { get; set; }


        public void ChangeType(BlockType type)
        {
            if(type == BlockType.Track && Type != BlockType.Track)
            {
                Tracks = new List<TrackPiece>();
            }
        }

        public void AddTrack(TrackPiece trackPiece)
        {
            //If track with same or opposing direction does not exist
            if(TrackCanBeAdded(trackPiece.Direction))
            {
                Tracks.Add(trackPiece);
            }
            else
            {
                throw new Exception("Cannot add track to this block");
            }
        }
        
        /// <summary>
        /// Checks if track with given direction can be added to block
        /// </summary>
        public bool TrackCanBeAdded(Direction direction)
        {
            return Type == BlockType.Track || Type == BlockType.Empty &&  GetTrack(direction) == null;
            
        }

        public TrackPiece GetTrack(Direction direction)
        {
            return Tracks.FirstOrDefault(track => track.Direction.Difference(direction) % 4 == 0);
        }
        

        /*
        /// <summary>
        /// Returns all tracks from this block, that can be entered from the given direction
        /// </summary>
        /// <param name="direction"></param>
        public List<TrackPiece> GetConnectedTracks(Direction direction)
        {
            return Tracks.Where(track => track.Direction.Difference(direction) != 3).ToList();
        }
        */
    }

    
}
