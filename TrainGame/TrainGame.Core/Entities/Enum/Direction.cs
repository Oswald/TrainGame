﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Geometry;

namespace TrainGame.Core.Entities.Enum
{
    public enum Direction
    {
        North,
        NorthEast,
        East,
        SouthEast,
        South,
        SouthWest,
        West,
        NorthWest
    }

    

    
    public static class DirectionMethods
    {
        public static List<Direction> AllDirections = new List<Direction>
        {
            Direction.North,
            Direction.NorthEast,
            Direction.East,
            Direction.SouthEast,
            Direction.South,
            Direction.SouthWest,
            Direction.West,
            Direction.NorthWest
        };

        public static Point VectorDirection(this Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    return new Point(0, -1);
                case Direction.NorthEast:
                    return new Point(1, -1);
                case Direction.East:
                    return new Point(1, 0);
                case Direction.SouthEast:
                    return new Point(1, 1);
                case Direction.South:
                    return new Point(0, 1);
                case Direction.SouthWest:
                    return new Point(-1, 1);
                case Direction.West:
                    return new Point(-1, 0);
                case Direction.NorthWest:
                    return new Point(-1, -1);
                default:
                    throw new Exception("Invalid direction");

            }

        }

        public static bool Diagonal(this Direction direction)
        {
            switch (direction)
            {

                case Direction.NorthEast:
                    return true;
                case Direction.SouthEast:
                    return true;
                case Direction.SouthWest:
                    return true;
                case Direction.NorthWest:
                    return true;
                default:
                    return false;

            }
        }

            public static float RadianDirection(this Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    return 4.7123f;
                case Direction.NorthEast:
                    return 5.4976f;
                case Direction.East:
                    return 0f;
                case Direction.SouthEast:
                    return 0.7854f;
                case Direction.South:
                    return 1.5708f;
                case Direction.SouthWest:
                    return 2.2561f;
                case Direction.West:
                    return 3.1414f;
                case Direction.NorthWest:
                    return 3.9269f;
                default:
                    throw new Exception("Invalid direction");

            }
        }

        public static Direction Opposite(this Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    return Direction.South;
                case Direction.NorthEast:
                    return Direction.SouthWest;
                case Direction.East:
                    return Direction.West;
                case Direction.SouthEast:
                    return Direction.NorthWest;
                case Direction.South:
                    return Direction.North;
                case Direction.SouthWest:
                    return Direction.NorthEast;
                case Direction.West:
                    return Direction.East;
                case Direction.NorthWest:
                    return Direction.SouthEast;
                default:
                    throw new Exception("Invalid direction");
            }
        }

        public static double Length(this Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    return 1;
                case Direction.NorthEast:
                    return 1.41;
                case Direction.East:
                    return 1;
                case Direction.SouthEast:
                    return 1.41;
                case Direction.South:
                    return 1;
                case Direction.SouthWest:
                    return 1.41;
                case Direction.West:
                    return 1;
                case Direction.NorthWest:
                    return 1.41;
                default:
                    throw new Exception("Invalid direction");
            }
        }

        public static int Difference(this Direction direction, Direction other)
        {
            var p1 = direction.VectorDirection();
            var p2 = other.VectorDirection();
            return (int)Math.Abs(p1.X - p2.X) + (int)Math.Abs(p1.Y - p2.Y);
        }
    }
}
