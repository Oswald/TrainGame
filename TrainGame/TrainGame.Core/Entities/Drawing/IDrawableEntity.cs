﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainGame.Core.Entities.Drawing
{
    public interface IDrawableEntity
    {
        DrawingInfo GetDrawingInfo();
    }
}
