﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Geometry;

namespace TrainGame.Core.Entities.Drawing
{
    public class DrawingInfo
    {
        public double X { get; }
        public double Y { get; }
        public float Angle { get; set; }
        public float Scale { get; set; }

        public DrawingInfo(double x, double y, float angle, float scale)
        {
            X = Config.BlockWidth * x;
            Y = Config.BlockWidth * y;
            Angle = angle;
            Scale = scale;
        }
    }
}
