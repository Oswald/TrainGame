﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Geometry;

namespace TrainGame.Core.Entities.Drawing
{
    public class TrackDrawingInfo : DrawingInfo
    {
        public bool Diagonal { get; }
        public TrackDrawingInfo(Point position, float angle, float scale, bool diagonal) : base(position, angle, scale)
        {
            Diagonal = diagonal;
        }
    }
}
