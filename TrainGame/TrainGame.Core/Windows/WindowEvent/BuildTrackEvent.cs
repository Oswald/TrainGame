﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Enum;
using TrainGame.Core.Entities.Geometry;

namespace TrainGame.Core.Windows.WindowEvent
{
    public class BuildTrackEvent : IWindowEvent
    {
        public Direction Direction { get; set; }
        public Point Location { get; set; }
    }
}
