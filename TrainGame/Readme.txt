Raiteet on solmuja. Yhteen liitettyjen raiteiden v�liin muodostuu kaari. 
- Ainoa erotus normaalista verkosta on yksisuuntaisuus: raiteella on kaksi p��t�. Liitokset tehd��n jompaan kumpaan, ja vain toisesta p��st� voi p��st� toiseen.

Vaikeat kysymykset:
- Mist� tunnistetaan, liittyv�tk� kiskot toisiinsa, kun niit� rakennetaan

- Jokainen raide muodostaa kaksi solmua:
	- Mist� tunnistetaan, kummast� p��dyst� juna on tullut sis��n
	- Flood-fill: samaa raidetta voidaan joutua k�ytt�m��n uudestaan. Pit�� huolehtia, ett� samaa p��ty� ei k�ytet� uudestaan


Junalla on reitti; lista kiskoista

Ikkunat:
	Ikkuna luodaan automaattisesti sen mukaan, mink�laista kamaa niihin laitetaan


Kartta:
	- Kamat lis�t��n karttaan
	- Ainoastaan palassa s�ilytet��n tieto siit�, mit� palan sis�ll� on
	- Jos pala on varattu radalle, palaan ei voi rakentaa taloa ja toisin p�in
