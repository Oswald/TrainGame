﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainGame
{
    public static class Config
    {
        public static Texture2D Spritesheet { get; set; }
        public static int ScreenWidth { get; set; }
        public static int ScreenHeight { get; set; }
    }
}
