﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Misc;
using TrainGame.State;
using TrainGame.State;

namespace TrainGame
{
    public class TrainGame : Game
    {

        SpriteBatch SpriteBatch { get; set; }
        GraphicsDeviceManager GraphicsDeviceManager { get; set; }

        IState State { get; set; }

        SpriteFont font;

        public TrainGame()
        {
            Config.ScreenHeight = 1080;
            Config.ScreenWidth = 1920;

            GraphicsDeviceManager = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferHeight = Config.ScreenHeight,
                PreferredBackBufferWidth = Config.ScreenWidth
            };
        }

        protected override void Initialize()
        {
            State = new GameState(new SpriteConfig());
            
            using (var stream = TitleContainer.OpenStream("Content/Spritesheet.png"))
            {
                Config.Spritesheet = Texture2D.FromStream(GraphicsDevice, stream);
            }

            font = Content.Load<SpriteFont>("Content/Font18");
           
            //var font36 = Content.Load<SpriteFont>("Content/Font36");

            //var asd = new GraphicsGandler(Content);

            
            
            this.IsMouseVisible = true;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void Update(GameTime gameTime)
        {
            var mouseState = Mouse.GetState();
            var keyboardState = Keyboard.GetState();
            State.Update(gameTime.ElapsedGameTime.TotalMilliseconds, keyboardState, mouseState);
        }


        protected override void Draw(GameTime gameTime)
        {

            GraphicsDevice.Clear(Color.LightGreen);
            SpriteBatch.Begin();
            State.Draw(SpriteBatch);

            /* var rect = new Texture2D(GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
             rect.SetData(new[] { Color.White });
             SpriteBatch.Draw(rect, new Vector2(400, 100), Color.Wheat);*/

            //Texture2D texture = new Texture2D(GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            //texture.SetData(new Color[] { Color.White });
            //SpriteBatch.Draw(texture, new Rectangle(700, 100, 10, 10), Color.Yellow);
            //SpriteBatch.DrawString(Globals.font36, text, position, Color.Black);


            //               Spritesheet  Piirtokohta        Paikka sheetissä                       //Kääntö   //Keskikohta         //Skaala               //Korkeus    
            //SpriteBatch.Draw(spritesheet, new Vector2(0, 0), new Rectangle(0, 0, 150, 150), Color.White, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.5f );
            //spriteBatch.Draw(Globals.spriteSheet, drawPos, image, Color.White, (float)angle, origin, scale, SpriteEffects.None, 0.5f);

            //SpriteBatch.DrawString(font, "asd", new Vector2(100, 100), Color.Wheat);
            //test.Draw(SpriteBatch, 1);
            SpriteBatch.End();
            base.Draw(gameTime);

        }
    }
}
