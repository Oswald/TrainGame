﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Drawing;
using TrainGame.Core.Entities.Tracks;
using TrainGame.Core.Entities.Trains;
using TrainGame.Drawables;

namespace TrainGame.Misc
{
    public  class SpriteConfig
    {
        Dictionary<Type, IDrawable> Sprites { get; }

        public  SpriteConfig()
        {
            Sprites = new Dictionary<Type, IDrawable>
            {
                { typeof(TrackPiece), new TrackSprite() },
                { typeof(TrainCar), new TrainCarSprite() }
            };

        }

        public void Draw(SpriteBatch spriteBatch, IDrawableEntity piece, float scale, double totalTime, Camera camera = null)
        {
            if (Sprites.ContainsKey(piece.GetType()))
            {
                var info = piece.GetDrawingInfo();
                if (info == null)
                {
                    return;
                }
                if (camera != null)
                {
                    camera.UpdateDrawingInfo(info);
                }
                Sprites[piece.GetType()].Draw(spriteBatch, info, scale, totalTime);
            }
            else
            {
                throw new Exception("Sprite not configured");
            }
        }

    }
}
