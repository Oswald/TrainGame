﻿
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Drawing;
using TrainGame.Core.Entities.Geometry;

namespace TrainGame.Misc
{
    public class Camera
    {
        int X { get; set; }
        int Y { get; set; }

        int ScreenHeight { get; }
        int ScreenWidth { get; }

        double zoom;
        public double Zoom { get { return Math.Pow(zoom, 2); } private set { zoom = value; } }

        public Camera(int x, int y)
        {
            X = x;
            Y = y;
            zoom = 1;
            ScreenHeight = Config.ScreenHeight;
            ScreenWidth = Config.ScreenWidth;
        }

        int GetMovement(double elapsedSeconds)
        {
            return (int)(elapsedSeconds / Zoom);
        }

        public void Update(KeyboardState keyboardState, double elapsedSeconds)
        {
            if (keyboardState.IsKeyDown(Keys.W))
            {
                Y -= GetMovement(elapsedSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.S))
            {
                Y += GetMovement(elapsedSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.A))
            {
                X -= GetMovement(elapsedSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.D))
            {
                X += GetMovement(elapsedSeconds);
            }
            if (keyboardState.IsKeyDown(Keys.Q))
            {
                zoom += elapsedSeconds / 1000;
                if (zoom > 3.5) zoom = 3.5f;
            }
            if (keyboardState.IsKeyDown(Keys.E))
            {
                zoom -= elapsedSeconds / 1000;
                if (zoom < 0.4) zoom = 0.4f;
            }

        }

        public double RelativeX(double x)
        {
            return (x - X) * Zoom + ScreenWidth / 2;
        }

        public double RelativeY(double y)
        {
            return (y - Y) * Zoom + ScreenHeight / 2;
        }

        public double RealX(double x)
        { // TODO:
            //return X + x - Zoom * GraphicsConfig.ScreenWidth / 2;
            return (x - ScreenWidth / 2) / Zoom + X;
        }

        public double RealY(double y)
        {
            return (y - ScreenHeight / 2) / Zoom + Y;
        }

        public void UpdateDrawingInfo(DrawingInfo info)
        {
            info.Position = new Point(RelativeX(info.Position.X), RelativeY(info.Position.Y));
            info.Scale = (float)Zoom;
        }


    }
}
