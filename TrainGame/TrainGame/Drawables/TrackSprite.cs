﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TrainGame.Core.Entities.Drawing;

namespace TrainGame.Drawables
{
    public class TrackSprite : Sprite
    {
        public Rectangle LongTrack { get; } = new Rectangle(0, 6, 70, 5);
        public Rectangle ShortTrack { get; } = new Rectangle(0, 0, 50, 5);
        
        public TrackSprite() : base()
        {

        }

        public override void Draw(SpriteBatch spriteBatch, DrawingInfo info, float scale, double totalTime)
        {
            var track = ((TrackDrawingInfo)info).Diagonal ? LongTrack : ShortTrack;
            spriteBatch.Draw(Config.Spritesheet, new Vector2((float)info.X, (float)info.Y), track, Color.White, info.Angle, new Vector2(track.Width / 2, track.Height / 2), info.Scale * scale * 1, SpriteEffects.None, 1);

        }
    }
}
