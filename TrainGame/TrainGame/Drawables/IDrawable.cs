﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGame.Core.Entities.Drawing;

namespace TrainGame.Drawables
{
    public interface IDrawable
    {
        void Draw(SpriteBatch spriteBatch, DrawingInfo info, float scale, double totalTime);
    }
}
