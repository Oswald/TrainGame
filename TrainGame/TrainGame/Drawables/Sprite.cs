﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using TrainGame.Core.Entities.Drawing;
using Microsoft.Xna.Framework;

namespace TrainGame.Drawables
{
    public class Sprite : IDrawable
    {
        Rectangle PositionInSheet { get; set; }
        Vector2 Origin { get; set; }
        float ImageScale { get; set; }
        float Height { get; set; }

        public Sprite(Rectangle positionInSheet, float imageScale, Vector2? origin = null, float height = 0.5f)
        {
            PositionInSheet = positionInSheet;
            Origin = origin ?? new Vector2(positionInSheet.Width / 2, positionInSheet.Height / 2);
            ImageScale = imageScale;
            Height = height;
        }

        public Sprite()
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch, DrawingInfo info, float scale, double totalTime)
        {
            spriteBatch.Draw(Config.Spritesheet, new Vector2((float)info.Position.X, (float)info.Position.Y), PositionInSheet, Color.White, info.Angle, Origin, scale * ImageScale, SpriteEffects.None, Height);
        }
    }
}
