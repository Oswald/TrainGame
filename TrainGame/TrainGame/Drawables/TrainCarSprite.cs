﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainGame.Drawables
{
    public class TrainCarSprite : Sprite
    {
        public TrainCarSprite() : base(new Rectangle(0, 12, 27, 5), 1)
        {

        }
    }
}
