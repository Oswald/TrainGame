﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TrainGame.Core.Entities.Drawing;
using TrainGame.Core.Entities.Tracks;
using TrainGame.Core.Entities.Geometry;
using TrainGame.Core.Entities.Enum;
using TrainGame.Misc;
using TrainGame.Core.Entities.Trains;
using TrainGame.Core.Entities.Map;

namespace TrainGame.State
{
    public class GameState : IState
    {
        List<IDrawableEntity> Entities { get { return TrackHandler.TrackPieces.Select(p => (IDrawableEntity)p).ToList(); } }
        Block[,] Map = new Block[100, 100];

        TrackHandler TrackHandler { get; }
        SpriteConfig SpriteConfig { get; }

        public List<TrainCar> Trains = new List<TrainCar>();

        public Camera Camera { get; set; }

        public GameState(SpriteConfig config)
        {
            Camera = new Camera(0, 0);
            Trains.Add(new TrainCar());
            TrackHandler = new TrackHandler(Map);

            var k = TrackHandler.ConstructTrack(0, 0, Direction.SouthEast);
            TrackHandler.ConstructTrack(1, 1, Direction.SouthEast);
            TrackHandler.ConstructTrack(2, 2, Direction.South);
            var t = TrackHandler.ConstructTrack(2, 2, Direction.East);
            


            var s = TrackHandler.FindRoute(k, t);
            //Trains[0].SetRoute(new Route(s.ToList()));

            //Entities.Add(new TrackPiece(new Point(50, 150), Direction.SouthEast));
            //Entities.Add(new TrackPiece(new Point(100, 100), Direction.SouthWest));
            //Entities.Add(new TrackPiece(new Point(100, 100), Direction.East));
            //Entities.Add(new TrackPiece(new Point(1, 100), Direction.East));
            //Entities.Add(new TrackPiece(new Point(200, 100), Direction.SouthWest));
            SpriteConfig = config;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach(var entity in Entities)
            {
                SpriteConfig.Draw(spriteBatch, entity, 1, 0, Camera);
            }
            foreach(var asd in Trains)
            {
                SpriteConfig.Draw(spriteBatch, asd, 1, 0, Camera);
            }
        }

        public void Update(double elapsedMilliseconds, KeyboardState keyboard, MouseState mouse)
        {
            Camera.Update(keyboard, elapsedMilliseconds);

            foreach(var car in Trains)
            {
                car.Update(elapsedMilliseconds);
            }
        }
    }
}
