﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainGame.State
{
    public interface IState
    {
        void Update(double elapsedMilliseconds, KeyboardState keyboard, MouseState mouse);

        void Draw(SpriteBatch spriteBatch);
    }
}
